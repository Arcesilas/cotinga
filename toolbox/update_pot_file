#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2006-2017 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Cotinga.

# Cotinga is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# Cotinga is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Cotinga; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import os
import sys
import subprocess

import toml
import polib

with open(os.path.join('pyproject.toml'), 'r') as f:
    pp = toml.load(f)

__myname__ = pp['tool']['poetry']['name']
__authors__ = pp['tool']['poetry']['authors']
__version__ = pp['tool']['poetry']['version']

__process_name = os.path.basename(__file__)
__abspath = os.path.abspath(__file__)
__l1 = len(__process_name)
__l2 = len(__abspath)
TOOLBOX_DIRNAME = 'toolbox/'
ROOTDIR = __abspath[:__l2 - __l1][:-(len(TOOLBOX_DIRNAME))]
DATADIR = os.path.join(ROOTDIR, 'cotinga', 'data')
LOCALEDIR = os.path.join(DATADIR, 'locale/')


os.chdir('..')


POT_FILE_PATH = LOCALEDIR + 'cotinga.pot'
UPDATES_PATH = LOCALEDIR + 'cotinga_updates.pot'

sys.stdout.write('Collecting translations...')

subprocess.run('xgettext --package-name="{}" --package-version="{}" '
               '--copyright-holder="{}" --from-code=UTF-8 '
               '--msgid-bugs-address="nh.techn@gmail.com" '
               '--keyword=tr --output={} '
               '`find ../{} -type f -name "*.py"`'
               .format(__myname__, __version__, __authors__,
                       os.path.join(LOCALEDIR,
                                    '{}_updates.pot'.format(__myname__)),
                       __myname__),
               shell=True, check=True)

sys.stdout.write(' done.\nLooking for updates...')
if not os.path.isfile(UPDATES_PATH):
    sys.stdout.write('\nNo updates file found. UPDATES_PATH={}\nExiting.\n'
                     .format(UPDATES_PATH))
    sys.exit(1)

main_pot_file = polib.pofile(POT_FILE_PATH)
updates_file = polib.pofile(UPDATES_PATH)
main_pot_file_msgids = [entry.msgid for entry in main_pot_file]
updates_file_msgids = [entry.msgid for entry in updates_file]
something_changed = False

# First, remove the msgids present in the old file and absent from
# the new one; remove the obsolete occurrences in main file entries,
# if necessary
to_delete = []
for main_entry in main_pot_file:
    if main_entry.msgid in updates_file_msgids:
        for updates_entry in updates_file:
            if updates_entry.msgid == main_entry.msgid:
                o_to_delete = []
                for o in main_entry.occurrences:
                    if o not in updates_entry.occurrences:
                        o_to_delete.append(o)
                        something_changed = True
                for o in o_to_delete:
                    sys.stdout.write('\nDeleting this occurrence: {} '
                                     'from this entry:\n{}\n\n'
                                     .format(str(o), str(main_entry.msgid)))
                    del main_entry.occurrences[
                        main_entry.occurrences.index(o)]

    else:  # This entry is absent from the update, so, remove it
        to_delete.append(main_entry)
        something_changed = True

for entry in to_delete:
    sys.stdout.write('\nRemoving this entry:\n{}\n\n'.format(entry))
    del main_pot_file[main_pot_file.index(entry)]

# Now, add new msgids and new occurrences of preexisting msgids
# in main_pot_file:
for updates_entry in updates_file:
    if updates_entry.msgid in main_pot_file_msgids:
        for main_entry in main_pot_file:
            if updates_entry.msgid == main_entry.msgid:
                for ref in updates_entry.occurrences:
                    if ref not in main_entry.occurrences:
                        sys.stdout.write('\nMerging this entry:\n{}\n\n'
                                         .format(updates_entry))
                        main_entry.merge(updates_entry)
                        main_entry.occurrences = list(
                            set(main_entry.occurrences))
                        something_changed = True
                        break

    else:  # This entry is absent from the main pot file, so add it
        sys.stdout.write('\nAdding new entry:\n{}\n\n'.format(updates_entry))
        main_pot_file.append(updates_entry)
        something_changed = True

if something_changed:
    sys.stdout.write('\nWriting changes to {}\n'.format(POT_FILE_PATH))
    main_pot_file.save(POT_FILE_PATH)
else:
    sys.stdout.write('  found nothing.\n')

# print('Removing {}.'.format(UPDATES_PATH))
os.remove(UPDATES_PATH)

sys.stdout.write('Done updating translations.\n')
