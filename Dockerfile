FROM ubuntu:cosmic

LABEL maintainer="nh.techn@gmail.com"

ARG BUILD_DIR

# Install third-party dependencies
RUN apt-get -qq update
RUN apt-get install -y curl gir1.2-gtk-3.0 libcairo2-dev libevince-dev libjpeg-dev libgif-dev libgirepository1.0-dev locales pkg-config python3.6 python3-pip python3-venv xauth
# python-gi-cairo
RUN apt-get clean

## Generate locales (required for the tests)
RUN locale-gen en_US.UTF-8 && locale-gen fr_FR.UTF-8

## Create directories for required files
RUN mkdir -p $BUILD_DIR

## Add files required for the build
COPY pyproject.toml pyproject.lock tox.ini README.rst ${BUILD_DIR}/
# CONTRIBUTORS.rst CHANGELOG.rst LICENSE MANIFEST.in  README
COPY cotinga ${BUILD_DIR}/cotinga/
COPY tests ${BUILD_DIR}/tests/

## Install poetry and cotinga
RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py > get-poetry.py \
    && python3.6 get-poetry.py \
    && cd ${BUILD_DIR} \
    && poetry develop
